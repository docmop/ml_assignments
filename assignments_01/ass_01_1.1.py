# %%
# Three features:
# ECONOMY

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

data = pd.read_csv("happiness-report-2016.csv", header=0)

# target feature
happinessScore = data.iloc[:, 3].values

# other features
economy = data.iloc[:, 6].values
family = data.iloc[:, 7].values
health = data.iloc[:, 8].values
freedom = data.iloc[:, 9].values
trust = data.iloc[:, 10].values
region = data.iloc[:, 1].values

X = data.iloc[:, [6, 7, 8, 9, 10]]  # economy, family, health, freedom, trust, region
X_full = pd.concat([X, pd.get_dummies(region)], axis=1)  # add region one-hot encoded


def linReg(x, y, x_label, y_label):
    reg = linear_model.LinearRegression()
    x_t = x.reshape(-1, 1)

    reg.fit(x_t, y)

    y_pred = reg.predict(x_t)

    # Calculate oefficients
    w0 = reg.intercept_
    w1 = reg.coef_[0]

    # Calculate MSE
    mse = mean_squared_error(y, y_pred)

    # Plot
    plt.scatter(x, y, color='blue', marker='x')
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot(x_t, y_pred, color='red', linewidth=2)
    plt.show()

    print("Linear Regression:")
    print("CorrCoeff:", np.corrcoef(x, y)[0][1])
    print("w0:", w0)
    print("w1:", w1)
    print("MSE:", mse)


def multiLinReg(X, y):
    # Split data into train and test data
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    # Fit linear model (multi linear model is automatically used here)
    reg = linear_model.LinearRegression()
    reg.fit(X_train, y_train)

    # Calculate oefficients
    print("Multiple Linear Regression:")
    print("w0:", reg.intercept_)
    print("w_i:", reg.coef_)

    # Calculate MSE
    mse = sum((reg.predict(X_test) - y_test)**2) / len(y_test)
    print("MSE:", mse)


# %%
linReg(economy, happinessScore, "Economy", "Happiness Score")
# ... add other feature plots

# %%
multiLinReg(X_full, happinessScore)


