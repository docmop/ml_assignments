import pandas as pd
import math
import numpy as np
from sklearn import linear_model
from sklearn.model_selection import KFold


def k_fold_multi_lienar_regression(X, y, splits=1):
    # Fit linear model (multi linear model is automatically used here)
    reg = linear_model.LinearRegression()

    # Prepare k-fold data splitting
    kf = KFold(n_splits=splits)

    rmse_sum = 0

    # Split, train and test
    for train_index, test_index in kf.split(X):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        reg.fit(X_train, y_train)
        rmse = math.sqrt(sum((reg.predict(X_test) - y_test) ** 2) / len(y_test))

        rmse_sum += rmse

    return rmse_sum / kf.get_n_splits(X)


def addFeature(X, y, feature, oneHot=False):
    if oneHot:
        Xi = pd.concat([X, pd.get_dummies(feature.values)], axis=1)
    else:
        Xi = pd.concat([X, feature], axis=1)

    return k_fold_multi_lienar_regression(Xi.values, y, 5)


data = pd.read_csv("fifa_clean.csv", header=0)

# A)
y = data.iloc[:, 50].values  # feature 'value'
X = data.iloc[:, [4, 6, 7, 10, 14, 15]]  # age, overall, potential, reputation, height, weight

print("Vanilla features:")
mrmse = k_fold_multi_lienar_regression(X.values, y, 5)
print(mrmse)

# B)
# Add:          29, 34, 16, 51
# Add 1-Hot:
# Add derived:
# Ruled out:    nationality, club

add_features = data.iloc[:, [51, 29, 34, 16]]

height = data.iloc[:, 14]
weight = data.iloc[:, 15]
bmi = weight / height**2

print("Additional features:")
print(addFeature(X, y, add_features))

print("Additional (bmi):")
print(addFeature(X, y, bmi))

Xi = pd.concat([X, add_features], axis=1)
Xi = pd.concat([Xi, bmi], axis=1)
print("All:")
print(k_fold_multi_lienar_regression(Xi.values, y, 5))

# candid = []
# for c in range(16, 50):
#     Xt = data.iloc[:, c]
#     candid.append([c, addFeature(X, y, Xt) - mrmse])
#
# i = 12
# Xt = data.iloc[:, i]
# candid.append([i, addFeature(X, y, Xt, oneHot=True) - mrmse])
#
# i = 9
# Xt = data.iloc[:, i]
# candid.append([i, addFeature(X, y, Xt, oneHot=True) - mrmse])
#
# i = 11
# Xt = data.iloc[:, i]
# candid.append([i, addFeature(X, y, Xt) - mrmse])
#
# print(sorted(candid, key=lambda x: x[1]))