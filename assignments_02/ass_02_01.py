import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.model_selection import KFold


def test_classification(y_test, y_pred):
    # Error rate
    print("Error Rate: " + str(1 - accuracy_score(y_true=y_test, y_pred=y_pred)))

    # Precision
    print("Precission (Micro): " + str(precision_score(y_true=y_test, y_pred=y_pred, average='micro')))
    print("Precission (Macro): " + str(precision_score(y_true=y_test, y_pred=y_pred, average='macro')))

    # Recall
    print("Recall (Micro): " + str(recall_score(y_true=y_test, y_pred=y_pred, average='micro')))
    print("Recall (Macro): " + str(recall_score(y_true=y_test, y_pred=y_pred, average='macro')))
    print()


def k_fold_logistic_regression(X, y, splits=1):
    # Fit linear model (multi linear model is automatically used here)
    lr = LogisticRegression()

    # Prepare k-fold data splitting
    kf = KFold(n_splits=splits)

    # Split, train and test
    for train_index, test_index in kf.split(X):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        lr.fit(X_train, y_train)
        y_pred = lr.predict(X_test)
        test_classification(y_test, y_pred)


data = pd.read_csv("letter-recognition.data", header=None)

y = data.iloc[:, 0].values
X = data.iloc[:, 1:].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

lr = LogisticRegression()
lr.fit(X_train, y_train)

# A) + B)
print("\nSINGLE:")
y_pred = lr.predict(X_test)
test_classification(y_test, y_pred)

# C)
print("\nK-FOLD:")
k_fold_logistic_regression(X, y, splits=7)
